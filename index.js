
// 1. Template Literals
let base = 2;
let cube = 3;
let getCube = `The cube of ${base} is ${base ** cube}`;
console.log(getCube);


// 2. Array Destructuring
let address = ["258", "Washington Ave NW", "California", 90011];

let [houseNum, street, state, code] = address;
console.log(`I live at ${houseNum} ${street}, ${state} ${code}`);


// 3. Object Destructuring	
let animal = {
	name: 'Lolong',
	type: 'saltwater crocodile',
	weight: 1075,
	length: "20 ft 3 in"
}

let {name, type, weight, length} = animal;

console.log(`${name} was a ${type}. He weighed at ${weight} kgs with a measurement of ${length}.`)


// 4. Arrow function
let numbers = [1,2,3,4,5];

numbers.forEach((number) => {
	console.log(number);
});



// 5. reduce array method
const sumNum = (total, number) => {
	return total += number;
}
let reduceNumber = numbers.reduce(sumNum);
console.log(reduceNumber);


// 6. Class Creation
class Dog{
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}

}

let myDog = new Dog("Kuma Bear", 4, "Chow Chow");
console.log(myDog);